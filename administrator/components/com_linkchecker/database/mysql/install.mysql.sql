DROP TABLE IF EXISTS `#__jsn_linkchecker`;

CREATE TABLE `#__jsn_linkchecker` (
	`id`       INT(11)     NOT NULL AUTO_INCREMENT,
	`title` VARCHAR(255) NOT NULL,
	`url` VARCHAR(255) NOT NULL,
	`string_test` TEXT ,
	`updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`)
)
	ENGINE =MyISAM
	AUTO_INCREMENT =0
	DEFAULT CHARSET =utf8;

drop TABLE if EXISTS `#__jsn_linkchecker_logs`;

CREATE table IF NOT EXISTS `#__jsn_linkchecker_logs` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`web_id` INT(11) NOT NULL ,
	`curl` TEXT NOT NULL ,
	`yahoo_api` LONGTEXT NOT NULL ,
	`header` TEXT NOT NULL ,
	`updated` TIMESTAMP DEFAULT current_timestamp,
	PRIMARY KEY (`id`)
) ENGINE =MyISAM
	AUTO_INCREMENT =0
	DEFAULT CHARSET =utf8;

INSERT INTO `#__jsn_linkchecker` (`title`, `url`) VALUES
('Demo JSN', 'http://demo.joomlashine.com');
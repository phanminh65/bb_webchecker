<?php
/**
 * @version    $Id$
 * @package    JSN_LinkChecker
 * @author     JoomlaShine Team <support@joomlashine.com>
 * @copyright  Copyright (C) 2012 JoomlaShine.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * Websites: http://www.joomlashine.com
 * Technical Support:  Feedback - http://www.joomlashine.com/contact-us/get-support.html
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * LinkChecker component helper.
 *
 * @package  JSN_LinkChecker
 * @since    1.0.0
 */
class JSNLinkCheckerHelper
{
	/**
	 * Configure the linkbar
	 *
	 * @param   string  $vName  The name of the active view
	 *
	 * @return	void
	 */
	public static function addSubmenu($vName)
	{

	}

	/**
	 * Add assets
	 *
	 * @return	void
	 */
	public static function addAssets()
	{
		// Load common assets
		! class_exists('JSNBaseHelper') OR JSNBaseHelper::loadAssets();
	}
}

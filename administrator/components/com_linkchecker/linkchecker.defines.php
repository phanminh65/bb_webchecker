<?php
/**
 * @version    $Id$
 * @package    JSN_LinkChecker
 * @author     JoomlaShine Team <support@joomlashine.com>
 * @copyright  Copyright (C) 2012 JoomlaShine.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * Websites: http://www.joomlashine.com
 * Technical Support:  Feedback - http://www.joomlashine.com/contact-us/get-support.html
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// Define dependencies
define('JSN_LINKCHECKER_DEPENDENCY', '[{"type":"plugin","folder":"system","name":"jsnframework","identified_name":"ext_framework","client":"site","publish":"1","lock":"1","title":"JSN Extension Framework System Plugin"}]');

// Define product identified name and version
define('JSN_LINKCHECKER_IDENTIFIED_NAME', 'ext_linkchecker');
define('JSN_LINKCHECKER_VERSION',         '@version');

// Define required Joomla version
define('JSN_LINKCHECKER_REQUIRED_JOOMLA_VER', '3.0');

// Define some necessary links
define('JSN_LINKCHECKER_INFO_LINK',   'http://www.joomlashine.com/joomla-extensions/jsn-linkchecker.html');
define('JSN_LINKCHECKER_DOC_LINK',    'http://www.joomlashine.com/joomla-extensions/jsn-linkchecker-docs.zip');
define('JSN_LINKCHECKER_REVIEW_LINK', 'http://www.joomlashine.com/joomla-extensions/jsn-linkchecker-on-jed.html');
define('JSN_LINKCHECKER_UPDATE_LINK', 'index.php?option=com_linkchecker&view=update');
